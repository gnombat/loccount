# Python: SLOC=14 LLOC=0
"""
This should mot be counted in SLOC
"""
import traceback


def print_separator():
    print('\n\n--------------------------------------------------\n')


def fatal_error(*args, trace=False):
    """Prints the traceback if trace is true, then *args and then exits."""
    print_separator()
    if trace:
        traceback.print_stack()
        print('\n\n')
    print(*args)
    exit(1)
